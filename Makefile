
# Usage: make release VERSION=0.0.1
release:
	sed --in-place='.tmp' -E 's|[0-9]+.[0-9]+.[0-9]+|$(VERSION)|' plugin.xml
	sed --in-place='.tmp' -E 's|[0-9]+.[0-9]+.[0-9]+|$(VERSION)|' package.json
	rm *.tmp
	git add plugin.xml package.json
	git commit -m 'Version $(VERSION)'
	git tag -a $(VERSION) -m 'Version $(VERSION)'
	git push
	npm publish .
