#import "THEHardRefresh.h"
#import "MainViewController.h"
#import "AppDelegate.h"

@implementation THEHardRefresh

- (BOOL)shouldOverrideLoadWithRequest:(NSURLRequest *)request navigationType:(WKNavigationType)type {
    if(type == WKNavigationTypeReload) {
    AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    UIViewController * oldViewController = appDelegate.window.rootViewController;

    // dismiss the old view controller:
    [oldViewController dismissViewControllerAnimated:NO completion:nil];

    // and set up the new view controller:
    appDelegate.viewController = [[MainViewController alloc] init];
    appDelegate.window.rootViewController = appDelegate.viewController;

    // Load whatever request we had into the new webViewEngine:
    [appDelegate.viewController.webViewEngine loadRequest:request];

    // Cancel the load in the old one (we just tossed it!)
    return NO;
  }

  // Normally, do nothing: (yes - permit this)
  return YES; 
}

@end
